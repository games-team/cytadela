/*
  File name: CCytadelaMenu.h
  Copyright: (C) 2004 - 2013 Tomasz Kazmierczak

  This file is part of Cytadela
  (an example of how NOT to design in-game menu)

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CCYTADELAMENU_H_
#define _CCYTADELAMENU_H_

#include "CSDLClass.h"
#include "Localization.h"
#include "CCytadelaConfig.h"

//menu numbers
typedef enum {
	MENU_MAIN = 0,
	MENU_LOAD,
	MENU_SETTINGS,
	MENU_TRAINING,
	MENU_INFO,
	MENU_VIDEO,
	MENU_GAME_SPEED,
	MENU_AUDIO,

	MENU_COUNT
} MenuLevel;

enum MenuImage {
	MIMG_MAIN = 0,
	MIMG_INFO,
	MIMG_CURSOR,
	MIMG_FONT,
	MIMG_INFOFONT,

	MIMG_IMAGES_COUNT
};

//enter() and runMenu() return values
enum MenuAction {
	MENUACT_EXIT,
	MENUACT_CONTINUE,
	MENUACT_START_GAME,
	MENUACT_LOAD_1,
	MENUACT_LOAD_2,
	MENUACT_LOAD_3,
	MENUACT_LOAD_4,
	MENUACT_LOAD_5,
	MENUACT_START_TR1,
	MENUACT_START_TR2,
	MENUACT_START_TR3,
	MENUACT_START_TR4,
	MENUACT_START_TR5
};

class CCytadelaMenu {
private:
	CSDLClass       *mSDL;
	CCytadelaConfig *mConfig;
	Localization    *mLocalization;
	MenuLevel        mMenu;
	uint32_t     mLastTime;
	uint32_t     mCursorFrame;
	uint32_t     mMenuImage[MIMG_IMAGES_COUNT];
	short        mMenuTextsXOffs[MENU_COUNT][6];
	char         mMenuTexts[MENU_COUNT][6][20];
	int32_t      mSelItem;

	//info menu related
	const char *mDefaultLangInfoText;
	const char *mOtherLangInfoText;
	const char *mInfoText;
	int32_t     mInfoTextHeight;
	float       mInfoTextPosition;
	bool        mScrollInfoText;

	void cursorUp();
	void cursorDown();
	MenuAction enter();
	void updateMenuScreen();
	void animateCursor();
	void initInfo();
	void displayInfo();
	void changeScreenSize();
	void toggleFullScreen();
	void changeGameSpeed();
	void changeMouseSpeed();
	void changeAudioStatus();

public:
	CCytadelaMenu();
	bool initMenu(const char **saveNames);
	void freeMenu();

	MenuAction runMenu();
};

#endif
