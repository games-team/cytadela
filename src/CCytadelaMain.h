/*
  File name: CCytadelaMain.h
  Copyright: (C) 2005 - 2010 Tomasz Kazmierczak

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CCYTADELAMAIN_H_
#define _CCYTADELAMAIN_H_

#include <stdint.h>
#include "CSDLClass.h"
#include "CytadelaSaveFile.h"
#include "CCytadelaGame.h"
#include "CInfoText.h"
#include "Localization.h"
#include "CCytadelaConfig.h"

class CCytadelaMain {
private:
	CSDLClass     *mSDL;
	const char    *mLevels[26];
	const char    *mSaveNames[21];
	const char    *mCplxNames[NumberOfComplexes];
	const char    *mCplxStatusName[4];
	GameState      mState;
	uint16_t       mCplxCounter;
	uint8_t        mCplxStatus[NumberOfComplexes];
	uint8_t        mLevel;
	uint8_t        mBeginLevel[7];
	uint8_t        mActualComplex;
	Localization  *mLocalization;
	CInfoText      mTexts[TEXT_CLASSES_COUNT];
	char           mSysDirPath[FILENAME_MAX];

	void animateMapCursor(uint32_t cursorImg, uint32_t *lastTime, uint32_t *cursorFrame, uint32_t num);
	bool saveScreen(uint32_t *imgNums);
	bool configScreen(CCytadelaConfig *config);
	bool startupScreen();
	bool controllersScreen();
	bool errorScreen(const char *errorText);
	bool chooseComplex();
	bool endLevel();
	bool gameOver();
	bool playIntro();
	bool playOutro();
	void reset(CCytadelaConfig *config);
	void setLevelTexts();

public:
	CCytadelaMain(const char *sysDirPath);

	void runMain();
};

#endif
