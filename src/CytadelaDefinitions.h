/*
  File name: CytadelaDefinitions.h
  Copyright: (C) 2008 - 2009 Tomasz Kazmierczak

  Creation date: 13.11.2008 20:57
  Last modification date: 13.06.2009

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CYTADELADEFINITIONS_H_
#define _CYTADELADEFINITIONS_H_

#include <stdint.h>

enum Level {
	LEVEL01A,
	LEVEL01B,
	LEVEL02A,
	LEVEL02B,
	LEVEL02C,
	LEVEL03A,
	LEVEL03B,
	LEVEL03C,
	LEVEL04A,
	LEVEL04B,
	LEVEL04C,
	LEVEL05A,
	LEVEL05B,
	LEVEL05C,
	LEVEL06A,
	LEVEL06B,
	LEVEL06C,
	LEVEL07A,
	LEVEL07B,
	LEVEL07C,
	LEVEL08A,
	TRAINING1,
	TRAINING2,
	TRAINING3,
	TRAINING4,
	TRAINING5,
	NO_LEVEL = 0xFF
};

enum Complex {
	CENTER,
	SEWERS,
	LABORATORY,
	STORES,
	POWER_PLANT,
	HANGAR,
	PRISON,
	NumberOfComplexes,
	Complex_FORCE_BYTE = 0xFF
};

enum ComplexStatus {
	INACTIVE,
	ACTIVE,
	FINISHED,
	CENTERINACTIVE,
	Status_FORCE_BYTE = 0xFF
};

enum WeaponTypes {
	HANDGUN = 0,
	SHOTGUN,
	MASHINEGUN,
	FLAMER,
	BLASTER,
	ROCKETLAUNCHER,
	NumberOfWeaponTypes
};

const uint32_t NumberOfSaves = 5;

#endif //_CYTADELADEFINITIONS_H_
