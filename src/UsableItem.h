/*
  File name: UsableItem.h
  Copyright: (C) 2007 - 2008 Tomasz Kazmierczak

  Creation date: 24.04.2007
  Last modification date: 15.12.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _USABLEITEM_H_
#define _USABLEITEM_H_

#include <stdint.h>
#include <math.h>

typedef enum {
	UIT_HAND,
	UIT_HANDGUN,
	UIT_SHOTGUN,
	UIT_MASHINEGUN,
	UIT_FLAMER,
	UIT_BLASTER,
	UIT_ROCKETLAUNCHER,
	UIT_RED_CARD,
	UIT_GREEN_CARD,
	UIT_BLUE_CARD
} UsableItemType;

typedef enum {
	WPNS_NA = 0,
	WPNS_ACTIVE,
	WPNS_DAMAGED
} WeaponState;

//clsses for item objects
class Item {
protected:
	uint32_t mTakeAmount;

public:
	int32_t  takeSoundNumber;

	Item(uint32_t takeAmount) : mTakeAmount(takeAmount), takeSoundNumber(0) {}

	//get
	uint32_t getTakeAmount() { return mTakeAmount; }
};

class Ammo : public Item {
private:
	uint32_t mAmount;

public:
	Ammo(uint32_t takeAmount);

	uint32_t getAmount() { return mAmount; }
	void setAmount(uint32_t amount);
	void take();
	void use() { mAmount--; }
};

//abstract class for usable items
class UsableItem : public Item {
protected:
	UsableItemType mType;

public:
	int32_t useSoundNumber;

	UsableItem(UsableItemType type, uint32_t takeAmount) :
			Item(takeAmount), mType(type), useSoundNumber(-1) {}
	virtual ~UsableItem() {}

	//get
	UsableItemType getType() { return mType; }
	virtual uint32_t getStatus() = 0;
	virtual uint32_t getAmountForCounter() = 0;
	//set
	virtual void setStatus(uint32_t) = 0;
};

//derivatives of UsableItem
class Hand : public UsableItem {
public:
	Hand() : UsableItem(UIT_HAND, 0) {}
	~Hand() {}

	//get
	uint32_t getStatus() { return WPNS_NA; }
	uint32_t getAmountForCounter() { return 0; }
	//set
	void setStatus(uint32_t) {}
};

class Card : public UsableItem {
public:
	uint32_t cardsCount;

	Card(UsableItemType type) : UsableItem(type, 1), cardsCount(0) {}
	~Card() {}

	//get
	uint32_t getStatus() { return WPNS_NA; }
	uint32_t getAmountForCounter() { return cardsCount; }
	//set
	void setStatus(uint32_t) {}
};

class Weapon : public UsableItem {
private:
	WeaponState mState;
	uint16_t    mBullets;
	uint16_t    mFireCount;
	int16_t     mPower;
	float       mRange;
	Ammo        mAmmo;

public:
	int32_t  fireSoundNumber;
	int32_t  noAmmoSoundNumber;

	Weapon(UsableItemType type, uint16_t bullets, int16_t power, float range, Ammo ammo);
	~Weapon() {}

	//get
	uint32_t getStatus() { return (uint32_t)mState; }
	uint16_t getBullets() { return mBullets; }
	int16_t getPower() { return mPower; }
	float getRange() { return mRange; }
	uint32_t getAmountForCounter() { return mAmmo.getAmount(); }
	Ammo *getAmmo() { return &mAmmo; }
	//set
	void setStatus(uint32_t status);

	void damage(uint16_t minshots, float randomnumber);
};

#endif //_USABLEITEM_H_
