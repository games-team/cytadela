/*
  File name: CInfoText.h
  Copyright: (C) 2005 - 2007 Tomasz Kazmierczak

  Creation date: 04.09.2005
  Last modification date: 31.08.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CINFOTEXT_H_
#define _CINFOTEXT_H_

#include <stdint.h>

#define BUMP_WALL_TEXTS      0
#define BUMP_DOOR_TEXTS      1
#define CARD_INSERT_TEXTS    2
#define CARD_WRONG_TEXTS     3
#define SLOT_USED_TEXTS      4
#define SLOT_BROKEN_TEXTS    5
#define NO_SLOT_TEXTS        6
#define HAND_NOTHING_TEXTS   7
#define HAND_SPRITE_TEXTS    8
#define HAND_WALL_TEXTS      9
#define HAND_BLOOD_TEXTS     10
#define HAND_SLOT_TEXTS      11
#define HAND_SLOT_USED_TEXTS 12
#define HAND_BLOCADE_TEXTS   13
#define HAND_EQUIPMENT_TEXTS 14
#define HAND_CORPS_TEXTS     15
#define BUTTON_TEXTS         16
#define BUMP_OPENING_TEXTS   17
#define HAND_ENEMY_TEXTS     18
#define HIT_TEXTS            19
#define MEDIPACK_TEXTS       20
#define POTION_TEXTS         21
#define BEER_TEXTS           22
#define PAUSE_ON_TEXTS       23
#define PAUSE_OFF_TEXTS      24
#define NO_CARDS_TEXTS       25
#define NO_AMMO_TEXTS        26
#define TEXT_CLASSES_COUNT   27

#define MAX_TEXTS_IN_CLASS   30

class CInfoText {
private:
	const char   *mTexts[MAX_TEXTS_IN_CLASS];
	int32_t       mActualText;
	int32_t       mTextsQuantity;

public:
	void init(const char *textsPointer[MAX_TEXTS_IN_CLASS], int32_t textsQuantity);
	void resetTextCounter() { mActualText = 0; }
	const char *getText();
};

#endif //_CINFOTEXT_H_
