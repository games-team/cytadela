/*
  File name: CInfoText.cpp
  Copyright: (C) 2005 - 2007 Tomasz Kazmierczak

  Creation date: 04.09.2005
  Last modification date: 31.08.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#include "CInfoText.h"

void CInfoText::init(const char *textsPointer[MAX_TEXTS_IN_CLASS], int32_t textsQuantity)
{
	for(int32_t i = 0; i < textsQuantity; ++i)
		mTexts[i] = textsPointer[i];
	mTextsQuantity = textsQuantity;
	mActualText = 0;
}

const char *CInfoText::getText()
{
	++mActualText;
	if(mActualText > mTextsQuantity)
		mActualText = 1;
	return mTexts[mActualText - 1];
}
