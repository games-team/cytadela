/*
  File name: CytadelaSaveFile.h
  Copyright: (C) 2005 - 2009 Tomasz Kazmierczak

  Creation date: 16.07.2005 01:50
  Last modification date: 10.06.2009

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CYTADELASAVEFILE_H_
#define _CYTADELASAVEFILE_H_

#include <stdint.h>
#include <assert.h>
#include "CytadelaDefinitions.h"

struct SaveStruct {
	uint16_t      weapons;
	uint16_t      ammo[NumberOfWeaponTypes];
	uint16_t      energy;
	uint16_t      cplxCounter;
	uint8_t       level;
	uint8_t       cplxStatus[NumberOfComplexes];
	uint8_t       actualComplex;
	uint8_t       bomb;
	bool          difficult;
	bool          visionNoise;
};

class CytadelaSaveFile {
	SaveStruct saveEntries[NumberOfSaves];

public:
	bool read(const char *sysDirPath);
	bool write(const char *sysDirPath);
	void reset();

	SaveStruct getSave(uint32_t entry)
	{
		assert(entry < NumberOfSaves);
		return saveEntries[entry];
	}

	void setSave(uint32_t entry, SaveStruct &save)
	{
		assert(entry < NumberOfSaves);
		saveEntries[entry] = save;
	}
};

#endif
