/*
  File name: farread.h
  Copyright: (C) 2005 - 2008 Tomasz Kazmierczak

  Creation date: 27.07.2005
  Last modification date: 23.11.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _FARREAD_H_
#define _FARREAD_H_

#include <stdint.h>

//archivised file formats
#define FAR_FMT_UNDET        -1
//images
#define FAR_FMT_TGA           3

//sounds/music
#define FAR_FMT_WAV          10
#define FAR_FMT_MP3          11

class FarHeader {
public:
	uint16_t fileId;             //file identifier
	uint16_t fileVer;            //file version
	uint32_t fileSize;           //file size
	int32_t  filesQuantity;      //number of archived files
	uint32_t reserved1;
	uint32_t reserved2;
};

class FarRootEntry {
public:
	uint32_t    packedFileOffset;   //file data offset
	uint32_t    packedFileSize;     //file size
	uint16_t    fileType;           //file type
	uint16_t    fileNameLength;     //file name length
	uint32_t    reserved1;
	uint32_t    reserved2;
};

typedef class tagImgDims {
public:
	uint32_t width;
	uint32_t height;
	uint32_t size;
	uint32_t bitsPerPixel;
} ImgDims;

//NOTE: memory pointed by the returned address must be freed by the caller
uint8_t *farReadImgData(const char *farFileName, const char *fileName, ImgDims &dims);

#endif
