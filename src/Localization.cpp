/*
  File name: Localization.cpp
  Copyright: (C) 2006 - 2013 Tomasz Kazmierczak

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#include <memory.h>
#include <stdio.h>
#include <cstdlib>
#include "Localization.h"

Localization *Localization::instance()
{
	static Localization inst;
	return &inst;
}

Localization::Localization() : mLocalizationFileBuffer(0), mInfoTexts(0), mError(LOC_NO_ERROR)
{
	memset(mLocalizationAvailable, 0, sizeof(mLocalizationAvailable));
}

Localization::~Localization()
{
	if(mLocalizationFileBuffer)
		delete[] mLocalizationFileBuffer;
}

void Localization::setDefaultLocalization()
{
	//set the default (Polish) localization
	if(not mLocalizationAvailable[LOC_MENU_START])
		mLocalizedTexts[LOC_MENU_START] = "START";
	if(not mLocalizationAvailable[LOC_MENU_LOAD])
		mLocalizedTexts[LOC_MENU_LOAD] = "WCZYTAJ";
	if(not mLocalizationAvailable[LOC_MENU_OPTIONS])
		mLocalizedTexts[LOC_MENU_OPTIONS] = "OPCJE";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING])
		mLocalizedTexts[LOC_MENU_TRAINING] = "TRENING";
	if(not mLocalizationAvailable[LOC_MENU_INFO])
		mLocalizedTexts[LOC_MENU_INFO] = "INFO";
	if(not mLocalizationAvailable[LOC_MENU_MENU])
		mLocalizedTexts[LOC_MENU_MENU] = "MENU";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING_1])
		mLocalizedTexts[LOC_MENU_TRAINING_1] = "PODZIEMIA";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING_2])
		mLocalizedTexts[LOC_MENU_TRAINING_2] = "MAGAZYNY 1";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING_3])
		mLocalizedTexts[LOC_MENU_TRAINING_3] = "MAGAZYNY 2";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING_4])
		mLocalizedTexts[LOC_MENU_TRAINING_4] = "LABORATORIA";
	if(not mLocalizationAvailable[LOC_MENU_TRAINING_5])
		mLocalizedTexts[LOC_MENU_TRAINING_5] = "KANAlY";
	if(not mLocalizationAvailable[LOC_MENU_DIFFICULTY_HARD])
		mLocalizedTexts[LOC_MENU_DIFFICULTY_HARD] = "TRUDNOsc DUzA";
	if(not mLocalizationAvailable[LOC_MENU_DIFFICULTY_EASY])
		mLocalizedTexts[LOC_MENU_DIFFICULTY_EASY] = "TRUDNOsc MAlA";
	if(not mLocalizationAvailable[LOC_MENU_VIDEO_OPTIONS])
		mLocalizedTexts[LOC_MENU_VIDEO_OPTIONS] = "OPCJE GRAFICZNE";
	if(not mLocalizationAvailable[LOC_MENU_FULLSCREEN])
		mLocalizedTexts[LOC_MENU_FULLSCREEN] = "PElNY EKRAN";
	if(not mLocalizationAvailable[LOC_MENU_WINDOWED])
		mLocalizedTexts[LOC_MENU_WINDOWED] = "OKNO";
	if(not mLocalizationAvailable[LOC_MENU_SPEED_SETTINGS])
		mLocalizedTexts[LOC_MENU_SPEED_SETTINGS] = "UST. SZYBKOsCI";

	if(not mLocalizationAvailable[LOC_GAME_WELCOME])
		mLocalizedTexts[LOC_GAME_WELCOME] = "WITAJ!";
	if(not mLocalizationAvailable[LOC_GAME_PAUSED])
		mLocalizedTexts[LOC_GAME_PAUSED] = "... GRA SPAUZOWANA ...";
	if(not mLocalizationAvailable[LOC_GAME_LOCATION])
		mLocalizedTexts[LOC_GAME_LOCATION] = "TWOJA LOKACJA";
	if(not mLocalizationAvailable[LOC_GAME_VISION_NOISE_ON])
		mLocalizedTexts[LOC_GAME_VISION_NOISE_ON] = "ZAKlUCENIA: WlaCZONE";
	if(not mLocalizationAvailable[LOC_GAME_VISION_NOISE_OFF])
		mLocalizedTexts[LOC_GAME_VISION_NOISE_OFF] = "ZAKlUCENIA: WYlaCZONE";
	if(not mLocalizationAvailable[LOC_GAME_YOU_ARE_DEAD])
		mLocalizedTexts[LOC_GAME_YOU_ARE_DEAD] = "NIESTETY WlAsNIE ZGINalEs...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_1])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_1] = "WZIalEs PISTOLET...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_2])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_2] = "WZIalEs STRZELBe...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_3])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_3] = "WZIalEs KARABIN MASZYNOWY...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_4])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_4] = "WZIalEs MIOTACZ OGNIA...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_5])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_5] = "WZIalEs BLASTER...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_WEAPON_6])
		mLocalizedTexts[LOC_GAME_TAKEN_WEAPON_6] = "WZIalEs WYRZUTNIe RAKIET...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_1])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_1] = "WZIalEs NABOJE...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_2])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_2] = "WZIalEs POCISKI DO STRZELBY...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_3])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_3] = "WZIalEs PAS Z AMUNICJa...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_4])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_4] = "WZIalEs PALIWO DO MIOTACZA...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_5])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_5] = "WZIalEs BATERIe DO BLASTERA...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_AMMO_6])
		mLocalizedTexts[LOC_GAME_TAKEN_AMMO_6] = "WZIalEs RAKIETe...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_CARD_1])
		mLocalizedTexts[LOC_GAME_TAKEN_CARD_1] = "WZIalEs CZERWONa KARTe...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_CARD_2])
		mLocalizedTexts[LOC_GAME_TAKEN_CARD_2] = "WZIalEs ZIELONa KARTe...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_CARD_3])
		mLocalizedTexts[LOC_GAME_TAKEN_CARD_3] = "WZIalEs NIEBIESKa KARTe...";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_1])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_1] = "ZNAZAZlEs PIERWSZa CZesc BOMBY!";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_2])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_2] = "ZNAZAZlEs DRUGa CZesc BOMBY!";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_3])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_3] = "ZNAZAZlEs JUz TRZECIa CZesc BOMBY!";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_4])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_4] = "ZNAZAZlEs CZWARTa CZesc BOMBY!";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_5])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_5] = "ZNAZAZlEs JUz PIaTa CZesc BOMBY!";
	if(not mLocalizationAvailable[LOC_GAME_TAKEN_BOMB_6])
		mLocalizedTexts[LOC_GAME_TAKEN_BOMB_6] = "ZNAZAZlEs OSTATNIa CZesc BOMBY!!!";
	if(not mLocalizationAvailable[LOC_GAME_CODE_WEAPONS])
		mLocalizedTexts[LOC_GAME_CODE_WEAPONS] = "PODARUNEK Z PIEKlA!";
	if(not mLocalizationAvailable[LOC_GAME_CODE_ENERGY])
		mLocalizedTexts[LOC_GAME_CODE_ENERGY] = "PAlER JEST TWoJ!";
	if(not mLocalizationAvailable[LOC_GAME_CODE_MAP])
		mLocalizedTexts[LOC_GAME_CODE_MAP] = "TERAZ WIDZISZ PRAWDe!";
	if(not mLocalizationAvailable[LOC_GAME_WEAPON_DAMAGED])
		mLocalizedTexts[LOC_GAME_WEAPON_DAMAGED] = "BROn USZKODZONA !!!";
	if(not mLocalizationAvailable[LOC_GAME_LOADING_LAUNCHER])
		mLocalizedTexts[LOC_GAME_LOADING_LAUNCHER] = "CZEKAJ! lADUJe MIOTACZ!";
	if(not mLocalizationAvailable[LOC_GAME_CLOSED_DOOR])
		mLocalizedTexts[LOC_GAME_CLOSED_DOOR] = "TO Sa ZAMKNIeTE DRZWI!";
	if(not mLocalizationAvailable[LOC_GAME_OPEN_DOOR])
		mLocalizedTexts[LOC_GAME_OPEN_DOOR] = "TO WYGLaDA JAK OTWARTE DRZWI...";
	if(not mLocalizationAvailable[LOC_GAME_OPENING_DOOR])
		mLocalizedTexts[LOC_GAME_OPENING_DOOR] = "OTWIERAJa SIe CZY ZAMYKAJa?!";
	if(not mLocalizationAvailable[LOC_GAME_HOT])
		mLocalizedTexts[LOC_GAME_HOT] = "AU! TO JEST GORaCE!";

	if(not mLocalizationAvailable[LOC_SAVE_DUNGEONS_1])
		mLocalizedTexts[LOC_SAVE_DUNGEONS_1] = "PODZIEMIA 1";
	if(not mLocalizationAvailable[LOC_SAVE_DUNGEONS_2])
		mLocalizedTexts[LOC_SAVE_DUNGEONS_2] = "PODZIEMIA 2";
	if(not mLocalizationAvailable[LOC_SAVE_POWER_PLANT_1])
		mLocalizedTexts[LOC_SAVE_POWER_PLANT_1] = "ELEKTROWNIA 1";
	if(not mLocalizationAvailable[LOC_SAVE_POWER_PLANT_2])
		mLocalizedTexts[LOC_SAVE_POWER_PLANT_2] = "ELEKTROWNIA 2";
	if(not mLocalizationAvailable[LOC_SAVE_POWER_PLANT_3])
		mLocalizedTexts[LOC_SAVE_POWER_PLANT_3] = "ELEKTROWNIA 3";
	if(not mLocalizationAvailable[LOC_SAVE_STORES_1])
		mLocalizedTexts[LOC_SAVE_STORES_1] = "MAGAZYNY 1";
	if(not mLocalizationAvailable[LOC_SAVE_STORES_2])
		mLocalizedTexts[LOC_SAVE_STORES_2] = "MAGAZYNY 2";
	if(not mLocalizationAvailable[LOC_SAVE_STORES_3])
		mLocalizedTexts[LOC_SAVE_STORES_3] = "MAGAZYNY 3";
	if(not mLocalizationAvailable[LOC_SAVE_HANGAR_1])
		mLocalizedTexts[LOC_SAVE_HANGAR_1] = "HANGAR 1";
	if(not mLocalizationAvailable[LOC_SAVE_HANGAR_2])
		mLocalizedTexts[LOC_SAVE_HANGAR_2] = "HANGAR 2";
	if(not mLocalizationAvailable[LOC_SAVE_HANGAR_3])
		mLocalizedTexts[LOC_SAVE_HANGAR_3] = "HANGAR 3";
	if(not mLocalizationAvailable[LOC_SAVE_LABORATORY_1])
		mLocalizedTexts[LOC_SAVE_LABORATORY_1] = "LABORATORIA 1";
	if(not mLocalizationAvailable[LOC_SAVE_LABORATORY_2])
		mLocalizedTexts[LOC_SAVE_LABORATORY_2] = "LABORATORIA 2";
	if(not mLocalizationAvailable[LOC_SAVE_LABORATORY_3])
		mLocalizedTexts[LOC_SAVE_LABORATORY_3] = "LABORATORIA 3";
	if(not mLocalizationAvailable[LOC_SAVE_SEWERS_1])
		mLocalizedTexts[LOC_SAVE_SEWERS_1] = "KANAlY 1";
	if(not mLocalizationAvailable[LOC_SAVE_SEWERS_2])
		mLocalizedTexts[LOC_SAVE_SEWERS_2] = "KANAlY 2";
	if(not mLocalizationAvailable[LOC_SAVE_SEWERS_3])
		mLocalizedTexts[LOC_SAVE_SEWERS_3] = "KANAlY 3";
	if(not mLocalizationAvailable[LOC_SAVE_PRISON_1])
		mLocalizedTexts[LOC_SAVE_PRISON_1] = "WIeZIENIE 1";
	if(not mLocalizationAvailable[LOC_SAVE_PRISON_2])
		mLocalizedTexts[LOC_SAVE_PRISON_2] = "WIeZIENIE 2";
	if(not mLocalizationAvailable[LOC_SAVE_PRISON_3])
		mLocalizedTexts[LOC_SAVE_PRISON_3] = "WIeZIENIE 3";
	if(not mLocalizationAvailable[LOC_SAVE_CENTER_1])
		mLocalizedTexts[LOC_SAVE_CENTER_1] = "CENTRUM 1";
	if(not mLocalizationAvailable[LOC_SAVE_EMPTY])
		mLocalizedTexts[LOC_SAVE_EMPTY] = "WOLNE";
	if(not mLocalizationAvailable[LOC_CPLX_SEWERS])
		mLocalizedTexts[LOC_CPLX_SEWERS] = "KANAlY      ";
	if(not mLocalizationAvailable[LOC_CPLX_LABORATORY])
		mLocalizedTexts[LOC_CPLX_LABORATORY] = "LABORATORIA ";
	if(not mLocalizationAvailable[LOC_CPLX_POWER_PLANT])
		mLocalizedTexts[LOC_CPLX_POWER_PLANT] = "ELEKTROWNIA ";
	if(not mLocalizationAvailable[LOC_CPLX_STORES])
		mLocalizedTexts[LOC_CPLX_STORES] = "MAGAZYNY    ";
	if(not mLocalizationAvailable[LOC_CPLX_HANGAR])
		mLocalizedTexts[LOC_CPLX_HANGAR] = "HANGAR      ";
	if(not mLocalizationAvailable[LOC_CPLX_PRISON])
		mLocalizedTexts[LOC_CPLX_PRISON] = "WIeZIENIE   ";
	if(not mLocalizationAvailable[LOC_CPLX_CENTER])
		mLocalizedTexts[LOC_CPLX_CENTER] = "CENTRUM     ";
	if(not mLocalizationAvailable[LOC_CPLX_STATUS_UNEXPLORED])
		mLocalizedTexts[LOC_CPLX_STATUS_UNEXPLORED] = "NIEZBADANY";
	if(not mLocalizationAvailable[LOC_CPLX_STATUS_NO_ACCESS])
		mLocalizedTexts[LOC_CPLX_STATUS_NO_ACCESS] = "NIEDOSTePNY";
	if(not mLocalizationAvailable[LOC_CPLX_STATUS_EXPLORED])
		mLocalizedTexts[LOC_CPLX_STATUS_EXPLORED] = "ZBADANY";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_COMPLEX])
		mLocalizedTexts[LOC_CPLX_MENU_COMPLEX] = "KOMPLEKS";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_STATUS])
		mLocalizedTexts[LOC_CPLX_MENU_STATUS] = "STATUS";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_CHOSEN])
		mLocalizedTexts[LOC_CPLX_MENU_CHOSEN] = "WYBRAlEs";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_NO_ACCESS_YET])
		mLocalizedTexts[LOC_CPLX_MENU_NO_ACCESS_YET] = "TEN KOMPLEKS JEST JESZCZE NIEDOSTePNY!";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_ALREADY_EXPLORED])
		mLocalizedTexts[LOC_CPLX_MENU_ALREADY_EXPLORED] = "TEN KOMPLEKS ZOSTAl JUz SPENETROWANY!";
	if(not mLocalizationAvailable[LOC_CPLX_MENU_NEED_WHOLE_BOMB])
		mLocalizedTexts[LOC_CPLX_MENU_NEED_WHOLE_BOMB] = "MUSISZ MIEc CAla BOMBe ABY TU WEJsc!";

	if(not mLocalizationAvailable[LOC_END_LEVEL])
		mLocalizedTexts[LOC_END_LEVEL] = "POZIOM";
	if(not mLocalizationAvailable[LOC_END_COMPLETED])
		mLocalizedTexts[LOC_END_COMPLETED] = "UKOnCZONY";
	if(not mLocalizationAvailable[LOC_END_KILLED])
		mLocalizedTexts[LOC_END_KILLED] = "ILOsc ZABITYCH PRZECIWNIKoW:";
	if(not mLocalizationAvailable[LOC_END_TIME])
		mLocalizedTexts[LOC_END_TIME] = "CZAS PRZECHODZENIA:";
	if(not mLocalizationAvailable[LOC_END_BOMB])
		mLocalizedTexts[LOC_END_BOMB] = "POSIADANE CZesCI BOMBY:";
	if(not mLocalizationAvailable[LOC_END_SAVE_QUESTION])
		mLocalizedTexts[LOC_END_SAVE_QUESTION] = "CZY CHCESZ ZAPISAc STAN GRY?";
	if(not mLocalizationAvailable[LOC_END_NO_YES])
		mLocalizedTexts[LOC_END_NO_YES] = "NIE         TAK";
	if(not mLocalizationAvailable[LOC_END_SAVE_CHOOSE_POS])
		mLocalizedTexts[LOC_END_SAVE_CHOOSE_POS] = "WYBIERZ POZYCJe DO ZAPISU";
	if(not mLocalizationAvailable[LOC_END_SAVE_QUIT])
		mLocalizedTexts[LOC_END_SAVE_QUIT] = "PONIECHAJ";

	if(not mLocalizationAvailable[LOC_BUMP_WALL]) {
		static const char *szBumpWall[] = {"OUCH!!!", "UWAzAJ TROCHe!!!",
			"NIE WCHODx W sCIANY!", "NIE MOzESZ PRZENIKAc PRZEZ sCIANY!",
			"OH, MOJA GlOWA!", "ARRRGHHHH...", "TY SlODKI BRUTALU...",
			"AAAH! TO BOLI!", "AU! NIE JESTEM DUCHEM!", "JAKA TWARDA sCIANA!",
			"AAAH! ZlAMAlEM NOS!", "WYBIlEs MI ZaB!", "UGH... MAM SINIAKA!",
			"BUM! AU! PRZESTAn!!!", "OSTROzNIEJ!", "OOPS... NIE RoB TEGO!",
			"WIeCEJ UWAGI!", "BLEBLEMLEEEE...", "OCH! KRWAWIe!",
			"AAA... JAKA STlUCZKA!", "BAM! ZAKlUCENIA WIZJI!", "PRZESTAn W KOnCU!",
			"TRACe ENERGIe!", "OOPS... CHYBA NIEDlUGO ZGINe...",
			"PRZEKLeTA sCIANA!", "AAAUAUA!!!", "NABIlES MI GUZA!"};
		mInfoTexts[BUMP_WALL_TEXTS].init(szBumpWall, 27);
	}
	if(not mLocalizationAvailable[LOC_BUMP_DOOR]) {
		static const char *szBumpDoor[] = {"TE DRZWI Sa ZAMKNIeTE!",
			"NIE PRZEJDZIESZ PRZEZ ZAMKNIeTE DRZWI!", "NAJPIERW JE OTWoRZ!",
			"OOPS! ZAMKNIeTE DRZWI!", "MOzE NAJPIERW JE OTWoRZ?"};
		mInfoTexts[BUMP_DOOR_TEXTS].init(szBumpDoor, 5);
	}
	if(not mLocalizationAvailable[LOC_CARD_INSERT]) {
		static const char *szCardInsert[] = {"KARTA WlOzONA!", "KARTA ZAAKCEPTOWANA!",
			"KARTA ZATWIERDZONA!", "WOW! ZADZIAlAlO!", "WlOzYlEM KARTe!",
			"KARTA OK! I CO TERAZ?", "HEJ! TEN SLOT POlKNal MI KARTe!"};
		mInfoTexts[CARD_INSERT_TEXTS].init(szCardInsert, 7);
	}
	if(not mLocalizationAvailable[LOC_CARD_WRONG]) {
		static const char *szCardWrong[] = {"ZlA KARTA! SPRoBUJ INNEJ!",
			"WRRR... BZZ... ZlA KARTA!", "NIEWlAsCIWY KOLOR KARTY!",
			"NIEODPOWIEDNIA KARTA!", "BlaD WALIDACJI KARTY! SPRoBUJ INNEJ!",
			"TA KARTA NIE PASUJE.", "NIESTETY TO JEST ZlA KARTA!",
			"BlaD. SPRoBUJ INNYCH KART.", "NIEWlAsCIWA KARTA!",
			"KARTA NIEWlAsCIWA LUB USZKODZONA!", "BleDNA KARTA. Wloz INNa!"};
		mInfoTexts[CARD_WRONG_TEXTS].init(szCardWrong, 11);
	}
	if(not mLocalizationAvailable[LOC_SLOT_USED]) {
		static const char *szSlotUsed[] = {"TEN SLOT JEST JUz W UzYCIU!",
			"NIE WlOzYSZ TU DRUGIEJ KARTY!", "SLOT ZAJeTY... ZNAJDx INNY!",
			"TEN SLOT JEST ZAJeTY!"};
		mInfoTexts[SLOT_USED_TEXTS].init(szSlotUsed, 4);
	}
	if(not mLocalizationAvailable[LOC_SLOT_BROKEN]) {
		static const char *szSlotBroken[] = {"SLOT USZKODZONY!", "SLOT NIECZYNNY!",
			"SLOT NA KARTY NIE DZIAlA!"};
		mInfoTexts[SLOT_BROKEN_TEXTS].init(szSlotBroken, 3);
	}
	if(not mLocalizationAvailable[LOC_NO_SLOT]) {
		static const char *szNoSlot[] = {"GDZIE NIBY CHCESZ TO WlOzYc?!",
			"NIE WIDZISZ SLOTU NA KARTe!", "POTRZEBUJESZ WOLNEGO SLOTU NA KARTe"};
		mInfoTexts[NO_SLOT_TEXTS].init(szNoSlot, 3);
	}
	if(not mLocalizationAvailable[LOC_HAND_NOTHING]) {
		static const char *szHandNothing[] = {"NIE WIDZe NIC CIEKAWEGO...",
			"NIE MA TU NIC DO ZBADANIA!", "TU JEST TYLKO POWIETRZE!",
			"TU NIC NIE MA!", "HMMM... NIC TU NIE WIDZe!", "PRZESTAn I IDx DALEJ!",
			"ZNAJDUJESZ TYLKO KURZ...", "TU NIE MA NIC CIEKAWEGO!",
			"NIE ZNAJDUJESZ NIC UzYTECZNEGO...",
			"PRZESZUKUJESZ OKOLICe DAREMNIE..."};
		mInfoTexts[HAND_NOTHING_TEXTS].init(szHandNothing, 10);
	}
	if(not mLocalizationAvailable[LOC_HAND_SPRITE]) {
		static const char *szHandSprite[] = {"TU JEST COs DZIWNEGO!", "CO TO JEST?!!",
			"NIE CHCe TEGO DOTKNac!", "NIE ZBADAM TEGO!",
			"NIE WYDAJE SIe TO SZKODLIWE...", "INTERESUJaCE...",
			"BOJe SIe TEGO DOTKNac!"};
		mInfoTexts[HAND_SPRITE_TEXTS].init(szHandSprite, 7);
	}
	if(not mLocalizationAvailable[LOC_HAND_WALL]) {
		static const char *szHandWall[] = {"MASZ PRZED SOBa sCIANe!",
			"WYGLaDA JAK NORMALNA sCIANA...", "TU NIE MA NIC NADZWYCZAJNEGO...",
			"NIE WYGLaDA INTERESUJaCO...", "sCIANA JAK sCIANA...",
			"NIE WCHODx W Ta sCIANe!", "PRZESTAn BADAc sCIANY!"};
		mInfoTexts[HAND_WALL_TEXTS].init(szHandWall, 7);
	}
	if(not mLocalizationAvailable[LOC_HAND_BLOOD]) {
		static const char *szHandBlood[] = {"FUJ! KREW NA sCIANIE!", "PElNO TU KRWI...",
			"TO CHYBA NIE MOJA KREW!", "KREW I KREW! JUz MI NIE DOBRZE!",
			"CHYBA KTOs TU ZGINal!"};
		mInfoTexts[HAND_BLOOD_TEXTS].init(szHandBlood, 5);
	}
	if(not mLocalizationAvailable[LOC_HAND_SLOT]) {
		static const char *szHandSlot[] = {"WYGLaDA JAK SLOT NA KARTe!",
			"MOzE SPRoBUJ WlOzYc KARTe?", "TO WOLNY SLOT NA KARTe!"};
		mInfoTexts[HAND_SLOT_TEXTS].init(szHandSlot, 3);
	}
	if(not mLocalizationAvailable[LOC_HAND_SLOT_USED]) {
		static const char *szHandSlotUsed[] = {"TO SLOT Z KARTa W sRODKU!",
			"TO ZABLOKOWANY SLOT NA KARTe!"};
		mInfoTexts[HAND_SLOT_USED_TEXTS].init(szHandSlotUsed, 2);
	}
	if(not mLocalizationAvailable[LOC_HAND_BLOCADE]) {
		static const char *szHandBlocade[] = {"TO BLOKUJE CI DROGe!",
			"UGH! TU Sa PLAMY KRWI!", "NIE PRZEJDZIESZ PRZEZ TO!",
			"TO NIE JEST INTERESUJaCE...", "NA PEWNO TEGO NIE DOTKNe!!!"};
		mInfoTexts[HAND_BLOCADE_TEXTS].init(szHandBlocade, 5);
	}
	if(not mLocalizationAvailable[LOC_HAND_EQUIPMENT]) {
		static const char *szHandEquipment[] = {"COs TU LEzY!",
			"WIDZISZ NA ZIEMI JAKIs PRZEDMIOT!", "TU JEST COs DO WZIeCIA!",
			"WEx TO I OBEJzYJ!", "SPRoBUJ WZIac TEN PRZEDMIOT!"};
		mInfoTexts[HAND_EQUIPMENT_TEXTS].init(szHandEquipment, 5);
	}
	if(not mLocalizationAvailable[LOC_HAND_CORPS]) {
		static const char *szHandCorps[] = {"FUJ! TO GNIJaCY TRUP!",
			"GNIJaCE MIeSO! UEEE...", "CHYBA ZARAZ ZWYMIOTUJe!", "STARE TRUCHlO!",
			"NIE DOTKNe TEGO!", "BLEEE... UMARLAK JAKIs...",
			"TO TYLKO CIAlO I KOsCI!", "UGH! sMIERDZaCE, STARE MIeSO!",
			"CZY TO JA ZROBIlEM?!!", "CO ZA OBRZYDLIWY WIDOK..."};
		mInfoTexts[HAND_CORPS_TEXTS].init(szHandCorps, 10);
	}
	if(not mLocalizationAvailable[LOC_BUTTON]) {
		static const char *szButton[] = {"GUZIK PRZESUNal SIe Z KLIKNIeCIEM!",
			"NACISNalEs GUZIK...", "... KLIK! ...", "OCH! GUZIK SIe PRZESUNal!",
			"NACISNalEM GO! I CO TERAZ?", "OOPS! TO DZIAlA!",
			"TO CHYBA NIE PUlAPKA?", "USlYSZAlEM SlABE KLIKNIeCIE!",
			"NA PEWNO COs SIe STAlO!", "UDAlO MI SIe GO PRZESUNac!"};
		mInfoTexts[BUTTON_TEXTS].init(szButton, 10);
	}
	if(not mLocalizationAvailable[LOC_BUMP_OPENING]) {
		static const char *szBumpOpening[] = {"POCZEKAJ Az DRZWI SIe OTWORZa!",
			"CZEKAJ! NIECH SIe NAJPIERW OTWORZa!"};
		mInfoTexts[BUMP_OPENING_TEXTS].init(szBumpOpening, 2);
	}
	if(not mLocalizationAvailable[LOC_HAND_ENEMY]) {
		static const char *szHandEnemy[] = {"WIDZISZ ATAKUJaCEGO CIe PRZECIWNIKA!",
			"NIE WYGLaDA PRZYJAxNIE!", "PRZESTAn I BROn SIe!",
			"TO TWoJ WRoG! BIJ GO!", "ZABIJ! ZABIJ! ZABIJ!",
			"CHYBA ON CIe NIE LUBI...", "WYCIaGNIJ SPLUWe I ZABIJ GO!"};
		mInfoTexts[HAND_ENEMY_TEXTS].init(szHandEnemy, 7);
	}
	if(not mLocalizationAvailable[LOC_HIT]) {
		static const char *szHit[] = {"DOSTAlEM!!!", "AAARRGHHH...", "DOSTAl MNIE...",
			"ACH! TO BOLI!", "ARGHH!", "UGH!", "AUAUA!!!", "OOPS...",
			"JESTEM RANNY!", "OHSSSS...", "OUCH! ZABIJ GO!", "TRACe ENERGIe!",
			"O KUR...", "OCH NIE! DOSTAlEM!", "AAAH...", "UUUGH...", "POMOCY!",
			"BLEEEE...", "O RANY!", "ALE MNIE..."};
		mInfoTexts[HIT_TEXTS].init(szHit, 20);
	}
	if(not mLocalizationAvailable[LOC_MEDIPACK]) {
		static const char *szMediPack[] = {"UzYlEs APTECZKI!",
			"ODZYSKAlEs TROCHe ENERGII!", "MMM... CZUJe SIe LEPIEJ!",
			"TAK! WIeCEJ ENERGII!", "ODZYSKAlEs SIlY!", "JAKA DAWKA ENERGII!",
			"CZUJe SIe O WIELE SILNIEJSZY!"};
		mInfoTexts[MEDIPACK_TEXTS].init(szMediPack, 7);
	}
	if(not mLocalizationAvailable[LOC_POTION]) {
		static const char *szPotion[] = {"UzYlEs PRZYSPIESZACZA!!!",
			"PORUSZASZ SIe TERAZ SZYBCIEJ!", "ZYSKAlEs DODATKOWa SZYBKOsc!"};
		mInfoTexts[POTION_TEXTS].init(szPotion, 3);
	}
	if(not mLocalizationAvailable[LOC_BEER]) {
		static const char *szBeer[] = {"KReCI MI SIe W GlOWIE...", "CHYBA SIe SPIlEM...",
			"OOPS... MDLI MNIE...", "HIC! MUSIAlEM SIe ZATRUc", "NIEDOBRZE MI..."};
		mInfoTexts[BEER_TEXTS].init(szBeer, 5);
	}
	if(not mLocalizationAvailable[LOC_PAUSE_ON]) {
		static const char *szPauseOn[] = {"... PAUZA ...", "GRA SPAUZOWANA!"};
		mInfoTexts[PAUSE_ON_TEXTS].init(szPauseOn, 2);
	}
	if(not mLocalizationAvailable[LOC_PAUSE_OFF]) {
		static const char *szPauseOff[] = {"WALKA ZNoW SIe ZACZYNA!", "PAUZA WYlaCZONA!",
			"IDx I GIn!", "GRA UAKTYWNIONA"};
		mInfoTexts[PAUSE_OFF_TEXTS].init(szPauseOff, 4);
	}
	if(not mLocalizationAvailable[LOC_NO_CARDS]) {
		static const char *szNoCards[] = {"NIE MASZ JUz TAKICH KART!",
			"BRAK JUz TAKICH KART!"};
		mInfoTexts[NO_CARDS_TEXTS].init(szNoCards, 2);
	}
	if(not mLocalizationAvailable[LOC_NO_AMMO]) {
		static const char *szNoAmmo[] = {"BRAK CI AMUNICJI!", "NIE MASZ JUz AMUNICJI!",
			"SKOnCZYlA CI SIe AMUNICJA!", "POTRZEBUJESZ AMUNICJI!"};
		mInfoTexts[NO_AMMO_TEXTS].init(szNoAmmo, 4);
	}

	if(not mLocalizationAvailable[LOC_MENU_SPEED_GAME])
		mLocalizedTexts[LOC_MENU_SPEED_GAME] = "GRA:";
	if(not mLocalizationAvailable[LOC_MENU_SPEED_MOUSE])
		mLocalizedTexts[LOC_MENU_SPEED_MOUSE] = "MYSZ:";

	if(not mLocalizationAvailable[LOC_GAME_GAME_SPEED])
		mLocalizedTexts[LOC_GAME_GAME_SPEED] = "SZYBKOsc GRY:";
	if(not mLocalizationAvailable[LOC_GAME_MOUSE_SPEED])
		mLocalizedTexts[LOC_GAME_MOUSE_SPEED] = "SZYBKOsc MYSZY:";

	if(not mLocalizationAvailable[LOC_YES])
		mLocalizedTexts[LOC_YES] = "TAK";
	if(not mLocalizationAvailable[LOC_NO])
		mLocalizedTexts[LOC_NO] = "NIE";

	if(not mLocalizationAvailable[LOC_MENU_AUDIO_SETTINGS])
		mLocalizedTexts[LOC_MENU_AUDIO_SETTINGS] = "OPCJE DxWIeKU";
}

bool Localization::loadLocalization(char *localization, CInfoText *infoTexts)
{
	mInfoTexts = infoTexts;
	if(mInfoTexts == 0)
	{
		mError = LOC_ERR_INVALID_ARGUMENT;
		return false;
	}
	//if trying to set already set localization
	if(strcmp(mCurrentLocalization, localization) == 0)
		return true;
	//if default localization chosen
	if(strcmp(localization, CYTADELA_DEFAULT_LOCALIZATION) == 0)
	{
		memset(mLocalizationAvailable, 0, sizeof(mLocalizationAvailable));
		setDefaultLocalization();
		sprintf(mCurrentLocalization, CYTADELA_DEFAULT_LOCALIZATION);
		return true;
	}

	char szLocFile[FILENAME_MAX];
	sprintf(szLocFile, "locale/%s.loc", localization);
	FILE *pLocFile = fopen(szLocFile, "rb");
	if(not pLocFile)
	{
		mError = LOC_ERR_CANNOT_OPEN_LOC_FILE;
		return false;
	}

	//check the file size
	fseek(pLocFile, 0, SEEK_END);
	size_t iFileSize = ftell(pLocFile);
	fseek(pLocFile, 0, SEEK_SET);

	//create a localization buffer in memory
	if(mLocalizationFileBuffer)
		delete[] mLocalizationFileBuffer;
	//make the buffer 1 byte larger than the file size so even if there is no newline
	//at the end of the file we will have the place for null-terminating character at
	//the end of the buffer (while processing the buffer we will change every newline
	//character into a null character - they will be the endings of the localization
	//strings)
	mLocalizationFileBuffer = new char[iFileSize + 1];
	if(not mLocalizationFileBuffer)
	{
		mError = LOC_ERR_OUT_OF_MEMORY;
		fclose(pLocFile);
		//we must set the default loclization, because the localization buffer
		//have been reallocated
		memset(mLocalizationAvailable, 0, sizeof(mLocalizationAvailable));
		setDefaultLocalization();
		return false;
	}
	//fill the buffer
	if(fread(mLocalizationFileBuffer, 1, iFileSize, pLocFile) < iFileSize)
	{
		mError = LOC_ERR_CANNOT_READ_LOC_FILE;
		fclose(pLocFile);
		delete[] mLocalizationFileBuffer;
		memset(mLocalizationAvailable, 0, sizeof(mLocalizationAvailable));
		setDefaultLocalization();
		return false;
	}
	fclose(pLocFile);
	//and the null-terminating character at the end
	//(the additional character at the end of the buffer)
	mLocalizationFileBuffer[iFileSize] = '\0';

	//now change every newline character and '@' to null character
	size_t i = 0;
	for(; i < iFileSize; i++)
		if(mLocalizationFileBuffer[i] == '\n' or mLocalizationFileBuffer[i] == '@')
			mLocalizationFileBuffer[i] = '\0';

	memset(mLocalizationAvailable, 0, sizeof(mLocalizationAvailable));
	//now fill the localized texts array with addresses to localized strings
	i = 0;
	LocalizedTexts eTextNumber;
	int32_t iMultipleTextsCount = 1;
	static const char *pMultipleTextsPointers[30];

	while(i < iFileSize)
	{
		//ommit every comment line
		if(mLocalizationFileBuffer[i] == '*')
		{
			while(mLocalizationFileBuffer[i] != '\0')
				++i;
			++i;
		}
		//localization text
		if(mLocalizationFileBuffer[i] == '#')
		{
			++i;
			//if next string of a multiple text
			if(mLocalizationFileBuffer[i] == '#')
			{
				++i;
				if(iMultipleTextsCount >= 30)
					continue;
				//copy pointer to the string
				pMultipleTextsPointers[iMultipleTextsCount] = &mLocalizationFileBuffer[i];
				//skip the string and null terminating character
				i += strlen(&mLocalizationFileBuffer[i]) + 1;
				iMultipleTextsCount++;
				continue;
			}
			else
			{
				//if an ending of a multiple text
				if(iMultipleTextsCount > 1 and (eTextNumber >= LOC_BUMP_WALL and eTextNumber <= LOC_NO_AMMO))
				{
					//add muliple texts to CInfoText class
					int32_t iTextClassNumber = eTextNumber - LOC_BUMP_WALL;
					pMultipleTextsPointers[0] = mLocalizedTexts[eTextNumber];
					mInfoTexts[iTextClassNumber].init(pMultipleTextsPointers, iMultipleTextsCount);
				}
				iMultipleTextsCount = 1;
			}
			char *szTextNumber = &mLocalizationFileBuffer[i];
			eTextNumber = (LocalizedTexts)atol(szTextNumber);
			//skip the text number and null terminating character
			i += 4;
			if(eTextNumber >= LOCALIZED_TEXTS_COUNT)
			{
				//skip the string and null terminating character
				i += strlen(&mLocalizationFileBuffer[i]) + 1;
				continue;
			}
			//copy pointer to the string
			mLocalizedTexts[eTextNumber] = &mLocalizationFileBuffer[i];
			//skip the string and null terminating character
			i += strlen(&mLocalizationFileBuffer[i]) + 1;
			mLocalizationAvailable[eTextNumber] = true;
			continue;
		}
		++i;
	}
	//if a multiple text was the last text in the localization file it was not added in the loop
	if(iMultipleTextsCount > 1 and (eTextNumber >= LOC_BUMP_WALL and eTextNumber <= LOC_NO_AMMO))
	{
		//add muliple texts to CInfoText class
		int32_t iTextClassNumber = eTextNumber - LOC_BUMP_WALL;
		pMultipleTextsPointers[0] = mLocalizedTexts[eTextNumber];
		mInfoTexts[iTextClassNumber].init(pMultipleTextsPointers, iMultipleTextsCount);
	}

	//set the default localization for strings that couldn't be localized to the
	//chosen language
	setDefaultLocalization();
	//set the current localization name
	sprintf(mCurrentLocalization, "%s", localization);
	return true;
}

int32_t Localization::getLastError()
{
	int32_t iErrorNumber = mError;
	mError = LOC_NO_ERROR;
	return iErrorNumber;
}

const char *getLocErrorDesc(int32_t errorNumber)
{
	switch(errorNumber)
	{
		case LOC_NO_ERROR:
			return "OK";
		case LOC_ERR_CANNOT_OPEN_LOC_FILE:
			return "Cannot open localization file";
		case LOC_ERR_CANNOT_READ_LOC_FILE:
			return "Cannot read from localization file";
		case LOC_ERR_OUT_OF_MEMORY:
			return "Out of memory";
		case LOC_ERR_INVALID_ARGUMENT:
			return "Invalid argument";
		default:
			return "Unknown error";
	}
	return 0;
}
